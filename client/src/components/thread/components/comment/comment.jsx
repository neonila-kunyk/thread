import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Label, Button, Form } from 'src/components/common/common';
import { ButtonType, IconName } from 'src/common/enums/enums';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const Comment = ({ comment, onCommentLike, onCommentDislike, onEdit, onDelete, userId }) => {
  const { id, body: commentBody, createdAt, user } = comment;
  const [isEditing, setIsEditing] = React.useState(false);
  const [body, setBody] = React.useState('');
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleEditComment = () => { setIsEditing(false); return onEdit({ id, body }); };
  const handleDeleteComment = () => onDelete(id);
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Content style={{ display: 'flex', justifyContent: 'space-between' }}>
          <CommentUI.Content>
            <CommentUI.Author as="a">{user.username}</CommentUI.Author>
            <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
          </CommentUI.Content>
          {userId === user.id && (
            <CommentUI.Content>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => { setIsEditing(true); setBody(commentBody); }}
              >
                <Icon name={IconName.EDIT} />
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => handleDeleteComment(id)}
              >
                <Icon name={IconName.DELETE} />
              </Label>
            </CommentUI.Content>
          )}
          {' '}
        </CommentUI.Content>
        {!isEditing && <CommentUI.Text>{commentBody}</CommentUI.Text>}
        {isEditing && (
          <Form reply onSubmit={handleEditComment}>
            <Form.TextArea
              style={{ height: '40%' }}
              value={body}
              onChange={ev => setBody(ev.target.value)}
            />
            <Button type={ButtonType.SUBMIT} isPrimary>
              Post comment
            </Button>
          </Form>
        )}
      </CommentUI.Content>
      <CommentUI.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {/* {likeCount} */}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {/* {dislikeCount} */}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  userId: commentType.isRequired
};

export default Comment;
