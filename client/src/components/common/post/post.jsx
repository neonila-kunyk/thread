import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Button, Form } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDislike, onExpandedPostToggle, sharePost, onEdit, onDelete, userId }) => {
  const {
    id,
    image,
    body: postBody,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);
  const [isLiked, setIsLiked] = React.useState(false);
  const [isDisliked, setIsDisliked] = React.useState(false);
  const [diffLike, setDiffLike] = React.useState(0);
  const [diffDislike, setDiffDislike] = React.useState(0);
  const [isStartState, setIsStartState] = React.useState(true);
  const [isEditing, setIsEditing] = React.useState(false);
  const [body, setBody] = React.useState('');

  const handlePostLike = () => {
    if (isStartState) {
      setDiffLike(1);
      setIsLiked(true);
      setIsStartState(false);
    } else if (!isLiked) {
      setDiffLike(1);
      setDiffDislike(-1);
      setIsLiked(true);
      setIsDisliked(false);
    } else {
      setDiffLike(-1);
      setIsLiked(false);
    }
    return onPostLike({ id, diffLike, diffDislike });
  };
  const handlePostDislike = () => {
    if (isStartState) {
      setDiffDislike(1);
      setIsDisliked(true);
      setIsStartState(false);
    } else if (isDisliked) {
      setDiffLike(-1);
      console.log(diffLike);
      setDiffDislike(1);
      setIsLiked(false);
      setIsDisliked(true);
    } else {
      setDiffDislike(-1);
      setIsDisliked(false);
    }
    return onPostDislike({ id, diffLike, diffDislike });
  };
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditPost = () => { setIsEditing(false); return onEdit({ id, body }); };
  const handleDeletePost = () => onDelete(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Content style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          {userId === user.id && (
            <Card.Content>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => { setIsEditing(true); setBody(postBody); }}
              >
                <Icon name={IconName.EDIT} />
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => handleDeletePost(id)}
              >
                <Icon name={IconName.DELETE} />
              </Label>
            </Card.Content>
          )}
        </Card.Content>
        {!isEditing && <Card.Description>{postBody}</Card.Description>}
        {isEditing && (
          <Form onSubmit={handleEditPost}>
            <Form.TextArea
              name="body"
              value={body}
              onChange={ev => setBody(ev.target.value)}
            />
            <div className={styles.btnWrapper}>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Post
              </Button>
            </div>
          </Form>
        )}
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  userId: postType.isRequired
};

export default Post;
