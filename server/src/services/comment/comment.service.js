class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  update(commentId, body) {
    return this._commentRepository.updateById(commentId, { body });
  }

  delete(commentId) {
    return this._commentRepository.deleteById(commentId);
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }
}

export { Comment };
